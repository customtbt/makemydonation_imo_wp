=== Make My Donation – In Memory Of Platform ===
Contributors: danillonunes
Tags: obituary, funeral home, charity, search, third-party, donation
Requires at least: 3.7
Tested up to: 4.4.2
Stable tag: 1.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Integrates your funeral home site with the Make My Donation IMO free service and allow donations to over 1.4 million eligible US charities through our platform.

== Description ==

Make My Donation – In Memory Of Platform ( https://funerals.makemydonation.org ) is a FREE service target for all funeral homes in United States, that provides full case management and reporting on all donation activity.

This plugin integrates your WordPress site with our In Memory Of API so with just one click you can enable donations in your obituary pages and have access to all our available charities and donation reports.


== Installation ==

1. Upload `makemydonation-imo` directory to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress
